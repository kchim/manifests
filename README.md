# manifests

## CI

[CI documentation](https://gitlab.com/redhat/edge/ci-cd/manifests/-/blob/main/ci/README.md)

## Deployment

[Deployment documentation](https://gitlab.com/redhat/edge/ci-cd/manifests/-/blob/main/deployment/README.md)

## Vagrant

The local environment provided by Vagrant is based on CentOS Stream 9.

Supported providers:

* libvirt
* virtualbox


### Starting

```vagrant
# libvirt
vagrant up --provider=libvirt

# virtualbox
vagrant up --provider=virtualbox

# using default provider from an env var
export VAGRANT_DEFAULT_PROVIDER="libvirt"

vagrant up
```

All files of this repository will be synced in `/vagrant` inside the crated virtual machine.

### Destroying

```vagrant
vagrant destroy
```

## CODEOWNERS

Specify which people or group to trigger an approval request.

This will ensure the MR to be approved by at least one people
who can give is expertise.

ℹ️ If you want to manage more file types for approval, please open an issue or contribute to the file! 👍

🌐 https://docs.gitlab.com/ee/user/project/code_owners.html

The local environment provided by Vagrant is based on CentOS Stream 9.
