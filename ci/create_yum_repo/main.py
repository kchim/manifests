#!/usr/bin/env python3

import logging
import logging.config
import os
import sys
import json
from concurrent import futures
from pathlib import Path

import shutil
import boto3
import requests
from jinja2 import Template
from requests.adapters import HTTPAdapter

import utils


def upload_directory(
    directory: str,
    prefix: str,
    bucket: str = os.environ["AWS_BUCKET_REPOS"],
    boto3_session: boto3.Session = boto3.Session(),
) -> int:
    ret = 0
    s3 = boto3_session.client("s3")

    input_dir = Path(directory)

    with futures.ThreadPoolExecutor() as executor:
        upload_task = {}

        for file in input_dir.rglob("*"):
            if file.is_dir():
                continue
            upload_task[
                executor.submit(upload_file, file, s3, bucket, prefix, input_dir)
            ] = file

        for task in futures.as_completed(upload_task):
            try:
                task.result()
            except Exception as e:
                logging.error(
                    f"Exception {e} encountered while uploading file"
                    f"{upload_task[task]}"
                )
                ret = 1
    return ret


def upload_file(
    file: Path, s3: boto3.client, bucket: str, prefix: str, directory: Path
) -> None:
    s3.upload_file(
        Filename=str(file.absolute()),
        Bucket=bucket,
        Key=prefix + "/" + str(file.relative_to(directory)),
        ExtraArgs={"ACL": "public-read"},
    )


def create_repo(repodir: str) -> int:
    return utils.run_cmd(cmd=["createrepo_c", repodir])


def test_repo(bucket: str, region: str, id: int, centos_version: int) -> int:
    with open("/etc/yum.repos.d/automotive.repo", "w") as repofile:
        repofile.write(
            Template(
                open(
                    os.path.join(
                        (os.path.dirname(os.path.realpath(__file__))),
                        "yumrepo.j2",
                    ),
                ).read()
            ).render(
                bucket=bucket, region=region, id=id, centos_version=centos_version
            ),
        )
    return utils.run_cmd(cmd=["dnf", "makecache", "--repo=auto-*"])


def download_package(
    package: str,
    output_dir: str,
    mirrors: list,
    repos: list,
    package_arch: str,
    http_session: requests.Session
) -> int:
    supported_arches = {
        "x86_64": ["x86_64", "i686", "noarch"],
        "aarch64": ["aarch64", "noarch"],
    }

    for url in mirrors:
        for repo in repos:
            for arch in supported_arches[package_arch]:
                package_filename = f"{package}.{arch}.rpm"
                resp = http_session.get(
                    url=(f"{url}{repo}/" f"{package_arch}/os/Packages/{package_filename}"),
                    allow_redirects=True,
                )
                if resp.status_code == 200:
                    # workaround: S3 doesn't like the character '+' at the URL (PATH or file names)
                    # It's a known issue:
                    # https://stackoverflow.com/questions/36734171/how-to-decide-if-the-filename-has-a-plus-sign-in-it
                    # The next line will rename the files with '+' with '-'. This is done before the
                    # 'createrepo_c' indexes the packages, so the repo's metadata will point
                    # to the files with the new names. dnf/yum and the osbuild can now download
                    # and install the packages.
                    package_filename = package_filename.replace("+", "-")
                    with open(
                        f"{output_dir}/{repo}/{package_arch}/os/{package_filename}", "wb"
                    ) as package_file:
                        package_file.write(resp.content)
                    return 0

    logging.error(
        f"{package} was not found, URL: {url}, REPO: {repo}, ARCH: {package_arch}"
    )
    return 1


def download_packages(
    packages_list_dir: str,
    centos_version: str,
    output_dir: str,
    arch: str,
    repos: list,
    mirrors: list,
) -> int:
    returnCode = 0

    for repo in repos:
        Path(f"{output_dir}/{repo}/{arch}/os").mkdir(parents=True)

    with open(
        f"{packages_list_dir}/{centos_version}-image-manifest.json", "r"
    ) as packages_file:
        json_data = json.load(packages_file)

    packages = (
        json_data[f"{centos_version}"]["arch"][arch]
        + json_data[f"{centos_version}"]["common"]
    )

    http_session = requests.Session()
    http_session.mount("http://", HTTPAdapter(max_retries=10))
    for package in packages:
        package = package.strip()
        x = download_package(package, output_dir, mirrors, repos, arch, http_session)
        if x != 0:
            returnCode = 1

    return returnCode


def main() -> int:
    log_config_path = Path(
        os.path.join(Path(__file__).parent.absolute(), "logging.conf")
    )

    logging.config.fileConfig(log_config_path)
    logging.getLogger("manifests")
    logging.info(":: create_yum_repo started ::")

    supported_arches = ["aarch64", "x86_64"]
    repositories = ["BaseOS", "AppStream", "extras"]

    centos_mirrors = {
        "cs8": [
            f"http://mirror.centos.org/centos/8-stream/",
            f"http://composes.centos.org/latest-CentOS-Stream-8/compose/",
        ],
        "cs9": [
            f"http://mirror.stream.centos.org/9-stream/",
            f"https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/",
        ],
    }

    packages_list_dir = sys.argv[1]
    pipeline_run_id = sys.argv[2]
    centos_versions = sys.argv[3:]

    logging.info(f"requested centos_versions: {centos_versions}")

    for version in centos_versions:
        logging.info(f"creating {version} repos")
        if version not in centos_mirrors:
            logging.error(
                f"unsupported/invalid value provided as parameter CENTOS_VERSION: {version}"
            )
            return 1

        repodir = f"/var/lib/repos/{version}"
        for arch in supported_arches:
            returnCode = download_packages(
                packages_list_dir,
                version,
                repodir,
                arch,
                repositories,
                centos_mirrors[version],
            )

            if returnCode:
                return 1

            # Show the list of downloaded packages
            packages_downloaded = []
            for repo in repositories:
                packages_downloaded += Path(f"{repodir}/{repo}/{arch}/os").glob("*.rpm")
            packages_downloaded.sort()
            logging.info(f"Packages downloaded: {len(packages_downloaded)}")

            for repo in repositories:
                returnCode = create_repo(f"{repodir}/{repo}/{arch}/os")
                if returnCode:
                    return 1

            returnCode = upload_directory(
                directory=repodir,
                prefix=f"{pipeline_run_id}/{version}",
            )
            if returnCode:
                return 1

            returnCode = test_repo(
                bucket=os.environ["AWS_BUCKET_REPOS"],
                region=os.environ["AWS_REGION"],
                id=pipeline_run_id,
                centos_version=version,
            )
            if returnCode:
                return 1

            with open("/etc/yum.repos.d/automotive.repo") as f:
                logging.debug(f.read())

            try:
                shutil.rmtree(repodir)
            except OSError as e:
                print("Error: %s - %s." % (e.filename, e.strerror))
    return 0


if __name__ == "__main__":
    sys.exit(main())
