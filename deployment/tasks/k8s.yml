---
- name: A-Team Pipeline | k8s | Validate vars are provided
  ansible.builtin.assert:
    that:
      - vars[item] != None
  with_items:
    - ocp_host
    - ocp_ns
    - ocp_key
    - deployment_repo_url
    - deployment_revision
    - gitlab_token
    - gitlab_key
    - ATEAM_AWS_ACCESS_KEY_ID
    - ATEAM_AWS_SECRET_ACCESS_KEY
    - ATEAM_AWS_BUCKET_REPOS
    - ATEAM_AWS_BUCKET_LOGS
    - ATEAM_AWS_BUCKET_REGION
    - HOST
    - SSH_PRIVATE_KEY
    - TF_API_KEY

- name: A-Team Pipeline | k8s | Create Tooling Container ImageStream
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: image.openshift.io/v1
      kind: ImageStream
      metadata:
        name: create_pipeline
        namespace: "{{ ocp_ns }}"
        annotations:
          openshift.io/display-name: create_pipeline
      spec:
        tags:
          - name: latest
        lookupPolicy:
          local: true

- name: A-Team Pipeline | k8s | Create Tooling Container BuildConfig
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: build.openshift.io/v1
      kind: BuildConfig
      metadata:
        name: create-pipeline-tooling
        namespace: "{{ ocp_ns }}"
      spec:
        runPolicy: Serial
        triggers:
          - type: "GitLab"
            gitlab:
              secret: "{{ gitlab_token }}"
          - type: "ConfigChange"
        source:
          git:
            uri: "https://gitlab.com/redhat/edge/ci-cd/manifests.git"
            ref: main
          contextDir: ./deployment/
        strategy:
          type: Docker
          dockerStrategy:
            env:
              - name: GIT_SSL_NO_VERIFY
                value: "true"
            dockerfilePath: Dockerfile
        output:
          to:
            kind: "ImageStreamTag"
            name: "create_pipeline:latest"

- name: A-Team Pipeline | k8s | Create create_yum_repo Container ImageStream
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: image.openshift.io/v1
      kind: ImageStream
      metadata:
        name: create_yum_repo
        namespace: "{{ ocp_ns }}"
        annotations:
          openshift.io/display-name: create_yum_repo
      spec:
        tags:
          - name: latest
        lookupPolicy:
          local: true

- name: A-Team Pipeline | k8s | Create create_yum_repo Container BuildConfig
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: build.openshift.io/v1
      kind: BuildConfig
      metadata:
        name: create-yum-repo-tooling
        namespace: "{{ ocp_ns }}"
      spec:
        runPolicy: Serial
        triggers:
          - type: "GitLab"
            gitlab:
              secret: "{{ gitlab_token }}"
          - type: "ConfigChange"
        source:
          git:
            uri: "https://gitlab.com/redhat/edge/ci-cd/manifests.git"
            ref: main
          contextDir: ./ci/create_yum_repo
        strategy:
          type: Docker
          dockerStrategy:
            env:
              - name: GIT_SSL_NO_VERIFY
                value: "true"
            dockerfilePath: Dockerfile
        output:
          to:
            kind: "ImageStreamTag"
            name: "create_yum_repo:latest"

- name: A-Team Pipeline | k8s | Openshift Pipelines Operator Subscription
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: operators.coreos.com/v1alpha1
      kind: Subscription
      metadata:
        name: openshift-pipelines-operator-rh
        namespace: openshift-operators
      spec:
        channel: stable
        installPlanApproval: Automatic
        name: openshift-pipelines-operator-rh
        source: redhat-operators
        sourceNamespace: openshift-marketplace
        startingCSV: redhat-openshift-pipelines.v1.5.2

- name: A-Team Pipeline | k8s | Wait for subscription to proceed
  kubernetes.core.k8s_info:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    kind: Subscription
    wait: yes
    name: openshift-pipelines-operator-rh
    namespace: openshift-operators
    wait_sleep: 10
    wait_timeout: 360

- name: A-Team Pipeline | k8s | Wait for tektonConfig to proceed
  kubernetes.core.k8s_info:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    kind: TektonConfig
    wait: yes
    name: config
    wait_sleep: 10
    wait_timeout: 360

- name: A-Team Pipeline | k8s | Openshift Pipelines Config
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: operator.tekton.dev/v1alpha1
      kind: TektonConfig
      metadata:
        name: config
      spec:
        targetNamespace: openshift-pipelines
        profile: all
        config: {}
        pipeline:
          disable-affinity-assistant: false
          disable-creds-init: false
          disable-home-env-overwrite: false
          disable-working-directory-overwrite: false
          enable-api-fields: stable
          enable-custom-tasks: false
          enable-tekton-oci-bundles: false
          metrics.pipelinerun.duration-type: histogram
          metrics.pipelinerun.level: pipelinerun
          metrics.taskrun.duration-type: histogram
          metrics.taskrun.level: taskrun
          require-git-ssh-secret-known-hosts: false
          running-in-environment-with-injected-sidecars: true
        pruner:
          resources:
            - pipelinerun
          keep: 30
          schedule: "*/5 * * * *"
        addon:
          params:
            - name: "clusterTasks"
              value: "true"
        dashboard: {}

- name: A-Team Pipeline | k8s | Set Subscription to Manual approval post-install
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: operators.coreos.com/v1alpha1
      kind: Subscription
      metadata:
        name: openshift-pipelines-operator-rh
        namespace: openshift-operators
      spec:
        channel: stable
        installPlanApproval: Manual
        name: openshift-pipelines-operator-rh
        source: redhat-operators
        sourceNamespace: openshift-marketplace
        startingCSV: redhat-openshift-pipelines.v1.5.2

- name: A-Team Pipeline | k8s | Create robot service account 
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: v1
      kind: ServiceAccount
      metadata:
        name: robot
        namespace: "{{ ocp_ns }}"

- name: A-Team Pipeline | k8s | Set policy role for robot service account 
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: rbac.authorization.k8s.io/v1
      kind: ClusterRoleBinding
      metadata:
        name: cluster-admin
        namespace: "{{ ocp_ns }}"
      roleRef:
        apiGroup: rbac.authorization.k8s.io
        kind: ClusterRole
        name: cluster-admin
      subjects:
      - kind: ServiceAccount
        name: robot
        namespace: "{{ ocp_ns }}"

- name: A-Team Pipeline | k8s | Secret
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: v1
      kind: Secret
      metadata:
        name: a-team
        namespace: "{{ ocp_ns }}"
      type: Opaque
      data:
        deployment_env: "{{ env | b64encode}}"
        deployment_namespace: "{{ ocp_ns | b64encode}}"
        deployment_repo_url: "{{ deployment_repo_url | b64encode }}"
        deployment_revision: "{{ deployment_revision | b64encode }}"
        gitlab_token: "{{ gitlab_token | b64encode }}"
        gitlab_key: "{{ gitlab_key | b64encode }}"
        ATEAM_AWS_ACCESS_KEY_ID: "{{ ATEAM_AWS_ACCESS_KEY_ID | b64encode }}"
        ATEAM_AWS_SECRET_ACCESS_KEY: "{{ ATEAM_AWS_SECRET_ACCESS_KEY | b64encode }}"
        ATEAM_AWS_BUCKET_REPOS: "{{ ATEAM_AWS_BUCKET_REPOS | b64encode }}"
        ATEAM_AWS_BUCKET_LOGS: "{{ ATEAM_AWS_BUCKET_LOGS | b64encode }}"
        ATEAM_AWS_BUCKET_REGION: "{{ ATEAM_AWS_BUCKET_REGION | b64encode }}"
        HOST: "{{ HOST | b64encode }}"
        SSH_PRIVATE_KEY: "{{ SSH_PRIVATE_KEY | b64encode }}"
        TF_API_KEY: "{{ TF_API_KEY | b64encode }}"
