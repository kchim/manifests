---
task_gitlab_verify_pipelinerun:
  params:
    - name: GITLAB_HOST_URL
      default: "gitlab.com"
    - name: API_PATH_PREFIX
      default: "/api/v4"
    - name: SHA
      type: string
    - name: MERGE_REQUEST_IID
      type: string
    - name: SOURCE_PROJECT_ID
      type: string
    - name: TARGET_PROJECT_ID
      type: string
    - name: DESCRIPTION
      type: string
    - name: STATE
      type: string
    - name: NAME
      type: string
  steps:
    - name: set-status
      image: registry.access.redhat.com/ubi8/python-39:1-15
      env:
        - name: GITLAB_TOKEN
          valueFrom:
            secretKeyRef:
              name: a-team
              key: gitlab_token
      script: |
        #!/usr/libexec/platform-python

        import os
        import requests
        import sys

        def set_status():
          params = {
              "state": STATE,
              "description": DESCRIPTION,
              "name": NAME
          }
          api_url = f"{API_PATH_PREFIX}/projects/{SOURCE_PROJECT_ID}/statuses/{SHA}"
          resp = requests.post(f"https://{GITLAB_HOST_URL}{api_url}", headers=headers, params=params)
          print(f"POST to {resp.url}")
          return (resp)

        def set_note():
          messageBody = f"***Note:*** Invite @auto-gitlab-bot as Developer on your fork project to enable the pipeline."
          params = {
            "id": TARGET_PROJECT_ID,
            "merge_request_iid": MERGE_REQUEST_IID,
            "body": messageBody
          }
          api_url = f"{API_PATH_PREFIX}/projects/{TARGET_PROJECT_ID}/merge_requests/{MERGE_REQUEST_IID}/notes"
          resp = requests.post(f"https://{GITLAB_HOST_URL}{api_url}", headers=headers, params=params)
          print(f"POST to {resp.url}")
          return (resp)

        GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
        MERGE_REQUEST_IID = "$(params.MERGE_REQUEST_IID)"
        SOURCE_PROJECT_ID = "$(params.SOURCE_PROJECT_ID)"
        TARGET_PROJECT_ID = "$(params.TARGET_PROJECT_ID)"
        GITLAB_HOST_URL = "$(params.GITLAB_HOST_URL)"
        API_PATH_PREFIX = "$(params.API_PATH_PREFIX)"
        SHA = "$(params.SHA)"
        STATE = "$(params.STATE)"
        DESCRIPTION = "$(params.DESCRIPTION)"
        NAME = "$(params.NAME)"

        headers = {
            "User-Agent": "TektonCD, the peaceful cat",
            "Authorization": f"Bearer {GITLAB_TOKEN}",
        }

        resp = set_status()
        if 200 <= resp.status_code <= 299:
            print(f"Just set status of {SOURCE_PROJECT_ID}#{SHA} to {STATE}")
        else:
            print(f"{resp.status_code} | Unable to set status")
            print(resp.text)
            # Send note if the status can't be set and always exit 1 for stopping the pipeline
            resp = set_note()
            if 200 <= resp.status_code <= 299:
                print(f"Just created a new note in {SOURCE_PROJECT_ID}")
                sys.exit(1)
            else:
                print(f"{resp.status_code} | Unable to set new note")
                print(resp.text)
                sys.exit(1)

task_pipeline_guard:
  params:
    - name: MAIN_TOOLCHAIN_BRANCH
      type: string
      default: "main"
    - name: MAIN_MANIFEST_BRANCH
      type: string
      default: "main"
  workspaces:
    - name: source
  results:
    - name: skip-image-build
      description: indicates if image build should be skipped or not
  steps:
    - name: run
      image: "registry.access.redhat.com/ubi8/python-39:1-15"
      script: |
        #!/usr/libexec/platform-python

        import os
        import pathlib
        import re
        import subprocess
        import sys

        # Files in the "manifests" repo which hosts the ci/cd and scripts which
        # are not relevant to the image build operations
        # Regex formatted strings to support dynamic matching
        IGNORED_TOOLCHAIN_REPO_FILES = [
            "\.gitignore",
            "LICENSE",
            "README\.md",
            "tform/.*"
        ]

        # Files in the "automotive-sig" repo (or other repos with manifest files)
        # which hosts the ci/cd which are not relevant to the image build operations
        # Regex formatted strings to support dynamic matching
        IGNORED_MANIFEST_REPO_FILES = [
            "\.gitignore",
            "docs/.*",
            "LICENSE",
            "mkdocs\.yml",
            "README\.md"
        ]

        def get_changed_files(repo_path, main_branch, ignored_files):
            # Fetch main branch references - Needed so we can compare main
            # branch against the version being built and find the merge-base
            subprocess.run(["git", "fetch", "origin", main_branch], check=True, cwd=repo_path)

            # Get the merge-base (only git 3.30+ supports the --merge-base
            # parameter with git-diff so we need to explicitly fetch it)
            merge_base = subprocess.run(
                ["git", "merge-base", "HEAD", f"origin/{main_branch}"],
                stdout=subprocess.PIPE,
                check=True,
                universal_newlines=True,
                cwd=repo_path
            )
            merge_base = merge_base.stdout.rstrip("\n")

            # Get the diff from the the local ancestor of the main branch
            print(f"Getting modified files in: {repo_path}")
            diff = subprocess.run(
                ["git", "diff", "--name-only", f"{merge_base}..HEAD"],
                stdout=subprocess.PIPE,
                check=True,
                universal_newlines=True,
                cwd=repo_path
            )
            diff_files = (diff.stdout).split()
            print(diff_files)

            # Remove files from the diff list that should not trigger image builds
            # The list of files to ignore are specified in the params
            changed_files = diff_files
            for ignore_regex in ignored_files:
                r = re.compile(ignore_regex)
                changed_files = list(filter(lambda x: not r.match(x), changed_files))

            return changed_files

        # Check for changes in toolchain repo
        toolchain_changed_files = get_changed_files(
          pathlib.Path("$(workspaces.source.path)/toolchain"),
          "$(params.MAIN_TOOLCHAIN_BRANCH)",
          IGNORED_TOOLCHAIN_REPO_FILES)
        print(f"toolchain_changed_files = {toolchain_changed_files}")

        # Check for changes in manifests repo
        manifest_changed_files = get_changed_files(
          pathlib.Path("$(workspaces.source.path)/manifest_repo"),
          "$(params.MAIN_TOOLCHAIN_BRANCH)",
          IGNORED_MANIFEST_REPO_FILES)
        print(f"manifest_changed_files = {manifest_changed_files}")

        # If list still has items, set result as needed
        with open("$(results.skip-image-build.path)", "w") as f:
            if not toolchain_changed_files and not manifest_changed_files:
                f.write("yes")
            else:
                f.write("no")

task_create_yum_repo:
  params:
    - name: PIPELINE_RUN_ID
    - name: CENTOS_VERSIONS
      type: array
  workspaces:
    - name: source
  steps:
    - name: run
      image: "image-registry.openshift-image-registry.svc:5000/{{ ocp_ns }}/create_yum_repo:latest"
      workingDir: "$(workspaces.source.path)/toolchain"
      envFrom:
        - secretRef:
            name: a-team
      env:
        - name: AWS_REGION
          value: eu-west-1
        - name: AWS_ACCESS_KEY_ID
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_ACCESS_KEY_ID
        - name: AWS_SECRET_ACCESS_KEY
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_SECRET_ACCESS_KEY
        - name: AWS_BUCKET_REPOS
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_BUCKET_REPOS
      args: ["$(params.CENTOS_VERSIONS[*])"]
      script: |
        #!/usr/bin/env bash
        set -xe
        ./ci/create_yum_repo/main.py \
            $(workspaces.source.path)/manifest_repo/package_list \
            $(params.PIPELINE_RUN_ID) \
            "$@"

# Runs in aarch64 baremetal machine. It doesn't support x86_64 yet
task_run_tmt_in_baremetal:
  params:
    - name: TMT_PLAN
      type: string
    - name: PIPELINE_RUN_ID
      type: string
  workspaces:
    - name: source
  steps:
    - name: run
      image: "quay.io/testing-farm/tmt:1.6.0"
      workingDir: "$(workspaces.source.path)/toolchain"
      envFrom:
        - secretRef:
            name: a-team
      env:
        - name: UUID
          value: $(params.PIPELINE_RUN_ID)
        - name: TMT_PLAN
          value: $(params.TMT_PLAN)
        - name: AWS_ACCESS_KEY_ID
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_ACCESS_KEY_ID
        - name: AWS_SECRET_ACCESS_KEY
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_SECRET_ACCESS_KEY
      script: |
        #!/usr/bin/env bash
        set -e
        mkdir -p -m 700 ~/.ssh/
        echo -e "${SSH_PRIVATE_KEY}" > ~/.ssh/id_rsa
        chmod 600 ~/.ssh/id_rsa
        EXIT_CODE=0
        cd $(workspaces.source.path)
        tmt run -vvv --all \
                --environment="UUID=${UUID}" \
                --environment="PIPELINE_RUN_ID=${PIPELINE_RUN_ID}" \
                --environment="AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}" \
                --environment="AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}" \
                provision --how connect --guest ${HOST} --key ~/.ssh/id_rsa \
                plan --name "${TMT_PLAN}" \
        || EXIT_CODE=1
        # Cleanup always the baremetal machine
        scp -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa ci/common/fix_baremetal.sh ${HOST}:
        ssh -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa ${HOST} ./fix_baremetal.sh
        exit ${EXIT_CODE}

task_run_in_testing_farm:
  params:
    - name: repo_url
      default: "https://gitlab.com/redhat/edge/ci-cd/manifests.git"
    - name: revision
    - name: REF
      default: "main"
    - name: TMT_PLAN
    - name: ARCH
    - name: PIPELINE_RUN_ID
    - name: PIPELINE_TASK_NAME
    - name: PREPROCESSOR_FILE
      default: "osbuild-manifests/osbuild-manifests/cs8/qemu/ostree-neptune-aarch64.mpp.json"
  workspaces:
    - name: source
  steps:
    - name: run
      image: "registry.access.redhat.com/ubi8:latest"
      envFrom:
        - secretRef:
            name: a-team
      env:
        - name: UUID
          value: $(params.PIPELINE_RUN_ID)
        - name: PREPROCESSOR_FILE
          value: $(params.PREPROCESSOR_FILE)
        - name: REPO_URL
          value: $(params.repo_url)
        - name: REVISION
          value: $(params.revision)
        - name: REF
          value: $(params.REF)
        - name: TMT_PLAN
          value: $(params.TMT_PLAN)
        - name: ARCH
          value: $(params.ARCH)
        - name: AWS_REGION
          value: eu-west-1
        - name: AWS_ACCESS_KEY_ID
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_ACCESS_KEY_ID
        - name: AWS_SECRET_ACCESS_KEY
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_SECRET_ACCESS_KEY
        - name: AWS_BUCKET_REPOS
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_BUCKET_REPOS
      script: |
        #!/usr/bin/env bash
        # Purposely don't let bash exit on error, we want to gracefully handle it
        $(workspaces.source.path)/toolchain/ci/common/testing-farm-request.sh
        RESULT="$?"
        echo "testing-farm-request.sh returned ${RESULT}"
        echo -n "${RESULT}" > testing-farm-request.exitcode

    - name: emit-pipeline-message
      image: "registry.access.redhat.com/ubi8:latest"
      envFrom:
        - secretRef:
            name: a-team
      env:
        - name: ARCH
          value: $(params.ARCH)
        - name: UUID
          value: $(params.PIPELINE_RUN_ID)
      script: |
        #!/usr/bin/env bash
        set -xe
        mkdir -p "$(workspaces.source.path)/messages"

        message_file="$(workspaces.source.path)/messages/$(params.PIPELINE_TASK_NAME)"
        echo "Writing to ${message_file}"

        RESULT=$(cat testing-farm-request.exitcode)
        if [[ "${RESULT}" -eq 0 ]]; then
          # Bit of a hack to hardcode these.
          # These variables are hardcoded in sync-to-aws.sh, so no dynamic way to fetch these
          S3_BUCKET_NAME="auto-ci-pipeline-images"
          AWS_REGION="eu-west-1"

          printf "**Download Image:** " \
            >> "${message_file}"
          printf "https://${S3_BUCKET_NAME}.s3.${AWS_REGION}.amazonaws.com/auto-osbuild-${ARCH}-${UUID}.raw\n\n" \
            >> "${message_file}"
          printf "**Download Metadata:** " \
            >> "${message_file}"
          printf "https://${S3_BUCKET_NAME}.s3.${AWS_REGION}.amazonaws.com/auto-osbuild-${ARCH}-${UUID}.json\n\n" \
            >> "${message_file}"
        fi

        if [[ -r URL ]]; then
          printf "**The build results are here:** " \
            >> "${message_file}"
          cat URL \
            >> "${message_file}"
        fi

        # Now return the original exitcode to finish the task
        exit "${RESULT}"

task_gitlab_set_status:
  params:
    - name: GITLAB_HOST_URL
      default: "gitlab.com"
    - name: API_PATH_PREFIX
      default: "/api/v4"
    - name: SHA
      default: $(params.git-revision)
    - name: PROJECT_ID
      default: $(params.project-id)
    - name: DESCRIPTION
      type: string
    - name: STATE
      type: string
    - name: SKIPPED
      type: string
      default: "no"
    - name: NAME
      type: string
    - name: TASKRUN_LOG_NAME
      type: string
      default: "#"
  steps:
    - name: set-status
      image: registry.access.redhat.com/ubi8/python-39:1-15
      env:
        - name: GITLAB_TOKEN
          valueFrom:
            secretKeyRef:
              name: a-team
              key: gitlab_token
        - name: AWS_BUCKET_LOGS
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_BUCKET_LOGS
        - name: AWS_REGION
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_BUCKET_REGION
      script: |
        #!/usr/libexec/platform-python

        import os
        import requests
        import sys

        GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
        AWS_BUCKET_LOGS = os.environ.get('AWS_BUCKET_LOGS')
        AWS_REGION = os.environ.get('AWS_REGION')
        PROJECT_ID = "$(params.PROJECT_ID)"
        GITLAB_HOST_URL = "$(params.GITLAB_HOST_URL)"
        API_PATH_PREFIX = "$(params.API_PATH_PREFIX)"
        SHA = "$(params.SHA)"
        STATE = "$(params.STATE)"
        SKIPPED = "$(params.SKIPPED)"
        DESCRIPTION = "$(params.DESCRIPTION)"
        NAME = "$(params.NAME)"
        TASKRUN_LOG_NAME = "$(params.TASKRUN_LOG_NAME)"
        TARGET_URL = f"https://{AWS_BUCKET_LOGS}.s3.{AWS_REGION}.amazonaws.com/{TASKRUN_LOG_NAME}"

        headers = {
            "User-Agent": "TektonCD, the peaceful cat",
            "Authorization": f"Bearer {GITLAB_TOKEN}",
        }


        # Convert raw Tekton pipeline results to GitLab pipeline version
        # This allows for setting the state directly as a param or using the
        # tekton result as a param
        # Supported GitLab states:
        # pending, running, success, failed, canceled
        if STATE == "Succeeded":
            STATE = "success"
        elif STATE == "Failed":
            STATE = "failed"
        elif STATE == "None":
            if SKIPPED == "yes":
                STATE = "success"
            else:
                STATE = "failed"

        params = {
            "state": STATE,
            "description": DESCRIPTION,
            "name": NAME
        }

        # Make log optional
        if TASKRUN_LOG_NAME != "#":
            params["target_url"] = TARGET_URL

        api_url = f"{API_PATH_PREFIX}/projects/{PROJECT_ID}/statuses/{SHA}"

        resp = requests.post(f"https://{GITLAB_HOST_URL}{api_url}", headers=headers, params=params)

        print(f"POST to {resp.url}")

        if 200 <= resp.status_code <= 299:
            print(f"Just set status of {PROJECT_ID}#{SHA} to {STATE}")
        else:
            print(f"{resp.status_code} | Unable to set status")
            print(resp.text)
            sys.exit(1)

task_gitlab_set_pipeline_note:
  params:
    - name: GITLAB_HOST_URL
      default: "gitlab.com"
    - name: API_PATH_PREFIX
      default: "/api/v4"
    - name: MERGE_REQUEST_IID
      type: string
    - name: PROJECT_ID
      type: string
    - name: PIPELINE_RUN_ID
      type: string
    - name: PIPELINE_RUN_NAME
      type: string
    - name: REVISION
      type: string
    - name: TASKS
      type: array
    - name: BUILD_SKIPPED
      type: string
  workspaces:
    - name: source

  steps:
    - name: get-pipelineruns-json
      image: "registry.redhat.io/openshift-pipelines/pipelines-cli-tkn-rhel8:0.19.0-3"
      script: |
        #!/usr/bin/env bash

        set -euo pipefail

        tkn pipelinerun describe "$(params.PIPELINE_RUN_NAME)" -n "{{ ocp_ns }}" -o json \
          > pipelineruns.json

    - name: set-note
      image: registry.access.redhat.com/ubi8/python-39:1-15
      env:
        - name: GITLAB_TOKEN
          valueFrom:
            secretKeyRef:
              name: a-team
              key: gitlab_token
        - name: AWS_BUCKET_LOGS
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_BUCKET_LOGS
        - name: AWS_REGION
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_BUCKET_REGION
      args: ["$(params.TASKS[*])"]
      script: |
        #!/usr/libexec/platform-python

        import json
        import os
        import pathlib
        import requests
        import sys
        import textwrap

        TASKS = sys.argv[1:]
        print(f"TASKS: {TASKS}")

        AWS_BUCKET_LOGS = os.environ.get('AWS_BUCKET_LOGS')
        AWS_REGION = os.environ.get('AWS_REGION')

        GITLAB_TOKEN = os.environ.get("GITLAB_TOKEN")
        GITLAB_HOST_URL = "$(params.GITLAB_HOST_URL)"
        API_PATH_PREFIX = "$(params.API_PATH_PREFIX)"
        PROJECT_ID = "$(params.PROJECT_ID)"
        MERGE_REQUEST_IID = "$(params.MERGE_REQUEST_IID)"
        PIPELINE_RUN_ID = "$(params.PIPELINE_RUN_ID)"
        PIPELINE_RUN_NAME = "$(params.PIPELINE_RUN_NAME)"
        REVISION = "$(params.REVISION)"
        BUILD_SKIPPED = "$(params.BUILD_SKIPPED)"

        headers = {
            "Content-Type": "application/json",
            "PRIVATE-TOKEN": f"{GITLAB_TOKEN}",
        }

        # Get overall state of tasks we care about
        task_states = {}
        with open("pipelineruns.json", "r") as pipelineruns:
          data = json.load(pipelineruns)

          for taskRunName in data["status"]["taskRuns"]:
            taskRun = data["status"]["taskRuns"][taskRunName]
            pipelineTaskName = taskRun["pipelineTaskName"]
            if pipelineTaskName not in TASKS:
              continue
            task_states[pipelineTaskName] = taskRun["status"]["conditions"][0]["reason"]

        # If state for a specified task is not found, set to failed or skipped
        for task in TASKS:
          if task not in task_states:
            if BUILD_SKIPPED == "yes":
              task_states[task] = "Skipped"
            else:
              print(f"Warning: Could not find task state for {task}")
              task_states[task] = "Not Run"

        print("Overall task states:")
        print(task_states)
        overall_state = "succeeded"
        if "Failed" in task_states.values() or "Not Run" in task_states.values():
          overall_state = "failed"

        # Create message
        # Note: Using double space to do soft line breaks
        messageBody = f"""
        # Pipeline {overall_state} for commit {REVISION}\n
        Task results and artifacts below.\n
        """

        log_url_base = f"https://{AWS_BUCKET_LOGS}.s3.{AWS_REGION}.amazonaws.com/{PIPELINE_RUN_NAME}"
        messages_dir = pathlib.Path("$(workspaces.source.path)/messages")
        for task in TASKS:
          task_message_file = messages_dir / task

          messageBody += f"### {task}: ***{task_states[task]}***\n"

          if task_message_file.exists():
            task_message = task_message_file.read_text()
            messageBody += task_message + "\n\n"

          if task_states[task] == "Not Run" or task_states[task] == "Skipped":
            messageBody += f"No task log available.\n"
          else:
            messageBody += f"Task Log: {log_url_base}_{task}.log\n"

          messageBody += "\n\n"

        messageBody += "***Warning:*** Artifacts will expire after a few days. Please download them as soon as possible :)\n"

        # Post note to GitLab MR
        params = {
          "id": PROJECT_ID,
          "merge_request_iid": MERGE_REQUEST_IID,
          "body": messageBody
        }

        api_url = f"{API_PATH_PREFIX}/projects/{PROJECT_ID}/merge_requests/{MERGE_REQUEST_IID}/notes"

        resp = requests.post(f"https://{GITLAB_HOST_URL}{api_url}", headers=headers, params=params)

        print(f"POST to {resp.url}")

        if 200 <= resp.status_code <= 299:
            print(f"Just created a new note in {PROJECT_ID}")
        else:
            print(f"{resp.status_code} | Unable to set new note")
            print(resp.text)
            sys.exit(1)

task_upload_taskrun_log:
  params:
    - name: TASK_NAME
    - name: PIPELINE_RUN_NAME
  steps:
    - name: fetch-log
      image: "registry.redhat.io/openshift-pipelines/pipelines-cli-tkn-rhel8:0.19.0-3"
      script: |
        #!/usr/bin/env bash

        set -euo pipefail

        microdnf install -y jq

        mkdir -p logs

        tkn pipelinerun describe "$(params.PIPELINE_RUN_NAME)" -n "{{ ocp_ns }}" -o json > pipelineruns.json
        taskrunname=$(cat pipelineruns.json | jq -r ".status.taskRuns | with_entries(select(.value.pipelineTaskName == \"$(params.TASK_NAME)\")) | keys[]")
        tkn taskrun logs "${taskrunname}" -n "{{ ocp_ns }}" > "logs/$(params.PIPELINE_RUN_NAME)_$(params.TASK_NAME).log"
        cat "logs/$(params.PIPELINE_RUN_NAME)_$(params.TASK_NAME).log"

    - name: upload-log
      image: "registry.redhat.io/openshift4/ose-tools-rhel8:v4.8.0-202110011559.p0.git.88e7eba.assembly.stream"
      envFrom:
        - secretRef:
            name: a-team
      env:
        - name: AWS_ACCESS_KEY_ID
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_ACCESS_KEY_ID
        - name: AWS_SECRET_ACCESS_KEY
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_SECRET_ACCESS_KEY
        - name: AWS_BUCKET_LOGS
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_BUCKET_LOGS
        - name: AWS_REGION
          valueFrom:
            secretKeyRef:
              name: a-team
              key: ATEAM_AWS_BUCKET_REGION
      script: |
        #!/usr/bin/env bash

        set -euo pipefail

        ls -al logs

        pip3 install awscli --upgrade --user
        export PATH=${PATH}:${HOME}/.local/bin

        aws s3 cp "logs/$(params.PIPELINE_RUN_NAME)_$(params.TASK_NAME).log" "s3://${AWS_BUCKET_LOGS}"
