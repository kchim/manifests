# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure('2') do |config|
  cpus = 2
  memory = 2048

  config.vm.box = 'centos/stream9'

  config.vm.provider :libvirt do |libvirt, override|
    override.vm.box_url = 'https://cloud.centos.org/centos/9-stream/x86_64/images/CentOS-Stream-Vagrant-9-20211111.1.x86_64.vagrant-libvirt.box'
    
    libvirt.cpus = cpus
    libvirt.memory = memory
    libvirt.uri = "qemu:///system"
    libvirt.machine_virtual_size = 20
  end

  config.vm.provider :virtualbox do |vb, override|
    override.vm.box_url = 'https://cloud.centos.org/centos/9-stream/x86_64/images/CentOS-Stream-Vagrant-9-20211111.1.x86_64.vagrant-virtualbox.box'
  
    vb.customize ["modifyvm", :id, "--memory", memory.to_s, "--cpus", cpus.to_s]
    config.disksize.size = "20GB"
  end

  config.vm.provision "shell", inline: <<-SHELL
    if [[ -e "/dev/vda1" ]]
    then
      echo ", +" | sudo sfdisk -N 1 /dev/vda --no-reread
      sudo partprobe
      sudo resize2fs /dev/vda1
    else
      echo ", +" | sudo sfdisk -N 1 /dev/sda --no-reread
      sudo partprobe
      sudo resize2fs /dev/sda1
    fi
  SHELL

  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "vagrant-ansible.yml"
  end
end
